import {
  CHOOSE_TAI_SUU,
  PLAY_GAME,
  TAI,
  XIU,
  YOU_LOSE,
  YOU_WIN,
} from "./xucSacConstant";

let innitialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      value: 1,
    },
    {
      img: "./imgXucSac/2.png",
      value: 2,
    },
    {
      img: "./imgXucSac/3.png",
      value: 3,
    },
  ],
  choice: null,
  result: "-",
  win: 0,
  total: 0,
};

export const XucSacReducer = (state = innitialState, action) => {
  switch (action.type) {
    case CHOOSE_TAI_SUU: {
      return { ...state, choice: action.payload };
    }
    case PLAY_GAME: {
      // Số lần chơi
      state.total++;

      // Đổ Xúc xắc
      let arrXucSac = [];
      //   Random 1 cục xúc sắc
      let randomize = () => {
        let number = Math.floor(Math.random() * 6) + 1;
        return { img: `./imgXucSac/${number}.png`, value: number };
      };
      // Push cục xúc sắc random vào array i = 3 lần
      for (let i = 0; i < 3; i++) {
        arrXucSac.push(randomize());
      }
      state.mangXucXac = arrXucSac;

      let result = 0;
      arrXucSac.forEach((item) => {
        result += item.value;
      });
      console.log("result: ", result);

      //   Kiểm tra chọn tài hay xỉu
      switch (state.choice) {
        case TAI: {
          if (result < 11) {
            state.result = YOU_LOSE;
          } else {
            state.result = YOU_WIN;
            state.win++;
          }
          break;
        }
        case XIU: {
          if (result < 11) {
            state.result = YOU_WIN;
            state.win++;
          } else {
            state.result = YOU_LOSE;
          }
          break;
        }
      }

      return { ...state };
    }
    default:
      return state;
  }
};
